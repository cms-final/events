package com.agh.cms.event.domain;

import com.agh.cms.common.domain.EventType;
import com.agh.cms.common.domain.UserBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import org.junit.Test;

import static com.agh.cms.event.utils.Requests.eventCreateRequest;
import static org.junit.Assert.*;

public class EventTest {

    private Event event = new Event(eventCreateRequest(), new UserBasicInfo("author", "java", false));

    @Test
    public void isDeleteionAllowed_whenEventIsNotBasic_returnsFalse() {
        EventCreateRequest request = eventCreateRequest();
        request.type = EventType.VACATION;
        event = new Event(request, new UserBasicInfo("author", "java", false));
        UserBasicInfo userBasicInfo = new UserBasicInfo("marcel", "java", true);

        boolean deletionAllowed = event.isDeletionAllowed(userBasicInfo);

        assertFalse(deletionAllowed);
    }

    @Test
    public void isDeletionAllowed_whenUserIsNotAuthorNorManager_returnsFalse() {
        UserBasicInfo userBasicInfo = new UserBasicInfo("marcel", "java", false);

        boolean deletionAllowed = event.isDeletionAllowed(userBasicInfo);

        assertFalse(deletionAllowed);
    }

    @Test
    public void isDeletionAllowed_whenUserIsNotAuthorNorInEventUsersAndInGroups_returnsFalse() {
        UserBasicInfo userBasicInfo = new UserBasicInfo("not in user", "not in group", true);

        boolean deletionAllowed = event.isDeletionAllowed(userBasicInfo);

        assertFalse(deletionAllowed);
    }

    @Test
    public void isDeletionAllowed_whenUserIsAuthor_returnsTrue() {
        UserBasicInfo userBasicInfo = new UserBasicInfo("author", "java", false);

        boolean deletionAllowed = event.isDeletionAllowed(userBasicInfo);

        assertTrue(deletionAllowed);
    }

    @Test
    public void isDeletionAllowed_whenUserIsInEventUsers_returnsTrue() {
        UserBasicInfo userBasicInfo = new UserBasicInfo("marcel", "not in group", true);

        boolean deletionAllowed = event.isDeletionAllowed(userBasicInfo);

        assertTrue(deletionAllowed);
    }

    @Test
    public void isDeletionAllowed_whenUserIsInEventGroups_returnsTrue() {
        UserBasicInfo userBasicInfo = new UserBasicInfo("not in user", "java", true);

        boolean deletionAllowed = event.isDeletionAllowed(userBasicInfo);

        assertTrue(deletionAllowed);
    }

    @Test
    public void isDeletionAllowed_whenUserIsInEventUsersAndGroups_returnsTrue() {
        UserBasicInfo userBasicInfo = new UserBasicInfo("marcel", "java", true);

        boolean deletionAllowed = event.isDeletionAllowed(userBasicInfo);

        assertTrue(deletionAllowed);
    }
}