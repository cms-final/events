package com.agh.cms.event.context.information;

import com.agh.cms.common.domain.UserBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import com.agh.cms.common.domain.exception.RabbitException;
import com.agh.cms.event.context.information.dto.EventNotification;
import com.agh.cms.event.domain.Event;
import com.agh.cms.event.domain.EventRepository;
import com.agh.cms.event.domain.NotificationsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.util.Collections;

import static com.agh.cms.event.utils.Requests.eventCreateRequest;
import static com.agh.cms.event.utils.Requests.eventCreateRequestWithUsersAndGroups;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.main.allow-bean-definition-overriding=true"})
@AutoConfigureMockMvc
public class EventCreateIntegrationTest {

    private static final String EVENTS = "/events";
    private final ObjectMapper mapper;

    private Event event;

    @MockBean
    private EventRepository eventRepository;

    @MockBean
    private UserBasicInfo userBasicInfo;

    @MockBean
    private NotificationsRepository notificationsRepository;

    @Autowired
    private MockMvc mockMvc;

    public EventCreateIntegrationTest() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Before
    public void setUp() {
        when(userBasicInfo.username()).thenReturn("author");
        event = new Event(eventCreateRequest(), userBasicInfo);
        when(eventRepository.findEventsDuringPeriodOfTime(any(LocalDateTime.class), any(LocalDateTime.class)))
                .thenReturn(Collections.singletonList(event));
        when(eventRepository.save(any(Event.class))).thenReturn(event);
    }
    
    @Test
    public void createEvent_whenEventRequestHasUsersThatHaveEvents_returns400() throws Exception {
        EventCreateRequest request = eventCreateRequestWithUsersAndGroups(Collections.singleton("marcel"), Collections.singleton("no event"));

        mockMvc.perform(MockMvcRequestBuilders.post(EVENTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"code\":\"400\",\"message\":\"marcel has event at this time\"}"));
    }

    @Test
    public void createEvent_whenEventRequestHasGroupsThatHaveEvents_returns400() throws Exception {
        EventCreateRequest request = eventCreateRequestWithUsersAndGroups(Collections.singleton("no event"), Collections.singleton("java"));

        mockMvc.perform(MockMvcRequestBuilders.post(EVENTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string ("{\"code\":\"400\",\"message\":\"java has event at this time\"}"));
    }

    @Test
    public void createEvent_whenEventRequestHasUsersAndGroupsThatHaveEvents_returns400() throws Exception {
        EventCreateRequest request = eventCreateRequestWithUsersAndGroups(Collections.singleton("marcel"), Collections.singleton("java"));

        mockMvc.perform(MockMvcRequestBuilders.post(EVENTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"code\":\"400\",\"message\":\"marcel has event at this time\"}"));
    }

    @Test
    public void createEvent_whenRabbitHasError_returns503() throws Exception {
        doThrow(new RabbitException("rabbit")).when(notificationsRepository).publish(any(EventNotification.class));
        EventCreateRequest request = eventCreateRequestWithUsersAndGroups(Collections.singleton("no event"), Collections.singleton("no event"));

        mockMvc.perform(MockMvcRequestBuilders.post(EVENTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isServiceUnavailable())
                .andExpect(content().string("{\"code\":\"503\",\"message\":\"rabbit\"}"));
    }

    @Test
    public void createEvent_whenEventRequestHasNotUsersNorGroupsThatHaveEvents_returns200() throws Exception {
        EventCreateRequest request = eventCreateRequestWithUsersAndGroups(Collections.singleton("no event"), Collections.singleton("no event"));

        mockMvc.perform(MockMvcRequestBuilders.post(EVENTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(event.basicInfo())));
    }
}