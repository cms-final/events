package com.agh.cms.event.utils;

import com.agh.cms.common.domain.EventType;
import com.agh.cms.common.domain.dto.EventCreateRequest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

public final class Requests {
    
    private static final LocalDateTime START_DATE = LocalDateTime.now().plusDays(1); 
    private static final LocalDateTime END_DATE = LocalDateTime.now().plusDays(2); 

    private Requests() {
    }

    public static EventCreateRequest eventCreateRequest() {
        EventCreateRequest eventCreateRequest = new EventCreateRequest();
        eventCreateRequest.title = "Title";
        eventCreateRequest.type = EventType.TRAINING;
        eventCreateRequest.startDate = START_DATE;
        eventCreateRequest.endDate = END_DATE;
        eventCreateRequest.users = Collections.singleton("marcel");
        eventCreateRequest.groups = Collections.singleton("java");
        return eventCreateRequest;
    }

    public static EventCreateRequest eventCreateRequestWithUsersAndGroups(Set<String> users, Set<String> groups) {
        EventCreateRequest eventCreateRequest = new EventCreateRequest();
        eventCreateRequest.title = "Title";
        eventCreateRequest.type = EventType.TRAINING;
        eventCreateRequest.startDate = START_DATE;
        eventCreateRequest.endDate = END_DATE;
        eventCreateRequest.users = users;
        eventCreateRequest.groups = groups;
        return eventCreateRequest;
    }
}
