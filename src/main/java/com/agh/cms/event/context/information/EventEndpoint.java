package com.agh.cms.event.context.information;

import com.agh.cms.common.domain.dto.EventBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import com.agh.cms.event.context.information.dto.EventExtendedInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/events")
class EventEndpoint {

    private final EventService eventService;

    EventEndpoint(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping
    ResponseEntity<List<EventBasicInfo>> eventsBasicInfo() {
        List<EventBasicInfo> events = eventService.getEventsBasicInfo();
        return ResponseEntity.ok(events);
    }

    @GetMapping("/{id}")
    ResponseEntity<EventExtendedInfo> eventExtendedInfo(@PathVariable int id) {
        EventExtendedInfo eventExtendedInfo = eventService.getEventExtendedInfo(id);
        return ResponseEntity.ok(eventExtendedInfo);
    }

    @GetMapping("/users/{username}")
    ResponseEntity<List<EventBasicInfo>> userEventsBasicInfo(@PathVariable String username) {
        List<EventBasicInfo> eventsByUsername = eventService.getEventsByUsername(username);
        return ResponseEntity.ok(eventsByUsername);
    }

    @GetMapping("/groups/{groupName}")
    ResponseEntity<List<EventBasicInfo>> groupsEventsBasicInfo(@PathVariable String groupName) {
        List<EventBasicInfo> eventsByGroupName = eventService.getEventsByGroupName(groupName);
        return ResponseEntity.ok(eventsByGroupName);
    }

    @PostMapping
    ResponseEntity<EventBasicInfo> createEvent(@Valid @RequestBody EventCreateRequest request) {
        EventBasicInfo event = eventService.createEvent(request);
        return ResponseEntity.ok(event);
    }

    @PostMapping("/internal")
    ResponseEntity<EventBasicInfo> createEventWithoutValidationAndNotification(@Valid @RequestBody EventCreateRequest request) {
        EventBasicInfo event = eventService.createEventWithoutValidationAndNotification(request);
        return ResponseEntity.ok(event);
    }

    @DeleteMapping("/{id}")
    HttpStatus deleteEvent(@PathVariable int id) {
        eventService.deleteEvent(id);
        return HttpStatus.OK;
    }
}
