package com.agh.cms.event.context.information.dto;

import java.util.Set;

public final class EventNotification {

    public String title;

    public Set<String> users;

    public Set<String> groups;
}
