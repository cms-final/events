package com.agh.cms.event.context.information.dto;

import com.agh.cms.common.domain.dto.EventBasicInfo;

import java.util.Set;

public final class EventExtendedInfo extends EventBasicInfo {

    public String description;

    public String author;

    public Set<String> users;

    public Set<String> groups;

    public boolean deletionAllowed;
}
