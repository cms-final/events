package com.agh.cms.event.context.information;

import com.agh.cms.common.domain.UserBasicInfo;
import com.agh.cms.common.domain.dto.EventBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import com.agh.cms.common.domain.exception.BusinessException;
import com.agh.cms.common.domain.exception.ResourceNotFoundException;
import com.agh.cms.common.infrastructure.exception.UnauthorizedException;
import com.agh.cms.event.context.information.dto.EventExtendedInfo;
import com.agh.cms.event.domain.Event;
import com.agh.cms.event.domain.EventRepository;
import com.agh.cms.event.domain.NotificationsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
class EventService {

    private final EventRepository eventRepository;
    private final UserBasicInfo userBasicInfo;
    private final NotificationsRepository notificationsRepository;

    EventService(EventRepository eventRepository, UserBasicInfo userBasicInfo, NotificationsRepository notificationsRepository) {
        this.eventRepository = eventRepository;
        this.userBasicInfo = userBasicInfo;
        this.notificationsRepository = notificationsRepository;
    }

    List<EventBasicInfo> getEventsBasicInfo() {
        return eventRepository.findAll().stream()
                .filter(Event::isEventBasicType)
                .map(Event::basicInfo)
                .collect(Collectors.toList());
    }

    EventExtendedInfo getEventExtendedInfo(int id) {
        return eventRepository.findById(id)
                .map(event -> event.extendedInfo(userBasicInfo))
                .orElseThrow(() -> new ResourceNotFoundException("Event not found"));
    }

    List<EventBasicInfo> getEventsByUsername(String username) {
        return eventRepository.findByUsername(username).stream()
                .map(Event::basicInfo)
                .collect(Collectors.toList());
    }

    List<EventBasicInfo> getEventsByGroupName(String groupName) {
        return eventRepository.findByGroupName(groupName).stream()
                .map(Event::basicInfo)
                .collect(Collectors.toList());
    }

    public EventBasicInfo createEvent(EventCreateRequest request) {
        validateEventsDuringPeriodOfTime(request);
        Event event = new Event(request, userBasicInfo);
        eventRepository.save(event);
        notificationsRepository.publish(event.eventNotification());
        return event.basicInfo();
    }

    EventBasicInfo createEventWithoutValidationAndNotification(EventCreateRequest request) {
        Event event = new Event(request, userBasicInfo);
        eventRepository.save(event);
        return event.basicInfo();
    }

    void deleteEvent(int id) {
        //todo: n+1
        Event event = eventRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Event not found"));
        if (!event.isDeletionAllowed(userBasicInfo)) {
            throw new UnauthorizedException();
        }
        eventRepository.delete(event);
    }

    private void validateEventsDuringPeriodOfTime(EventCreateRequest request) {
        List<Event> eventsDuringPeriodOfTime = eventRepository.findEventsDuringPeriodOfTime(request.startDate, request.endDate);
        request.users.forEach(username -> {
            if (eventsDuringPeriodOfTime.stream().anyMatch(event -> event.isUserInEvent(username))) {
                throw new BusinessException(username + " has event at this time");
            }
        });
        request.groups.forEach(groupName -> {
            if (eventsDuringPeriodOfTime.stream().anyMatch(event -> event.isGroupInEvent(groupName))) {
                throw new BusinessException(groupName + " has event at this time");
            }
        });
    }
}
