package com.agh.cms.event.domain;

import com.agh.cms.common.domain.EventType;
import com.agh.cms.common.domain.UserBasicInfo;
import com.agh.cms.common.domain.dto.EventBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import com.agh.cms.event.context.information.dto.EventExtendedInfo;
import com.agh.cms.event.context.information.dto.EventNotification;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.agh.cms.common.domain.EventType.*;

@Entity
@Access(AccessType.FIELD)
@Table(name = "event", schema = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "event_type")
    private EventType type;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "author")
    private String author;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id", referencedColumnName = "id", updatable = false)
    private Set<EventGroup> groups;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id", referencedColumnName = "id", updatable = false)
    private Set<EventUser> users;

    private Event() {
    }

    public Event(EventCreateRequest request, UserBasicInfo userBasicInfo) {
        //todo: preconditions
        this.title = request.title;
        this.description = request.description;
        this.type = request.type;
        this.startDate = request.startDate;
        this.endDate = request.endDate;
        this.author = userBasicInfo.username();
        this.users = Collections.unmodifiableSet(request.users.stream()
                .map(username -> new EventUser(this, username))
                .collect(Collectors.toSet()));
        this.groups = Collections.unmodifiableSet(request.groups.stream()
                .map(groupName -> new EventGroup(this, groupName))
                .collect(Collectors.toSet()));
    }

    public EventBasicInfo basicInfo() {
        EventBasicInfo eventBasicInfo = new EventBasicInfo();
        eventBasicInfo.id = id;
        eventBasicInfo.title = title;
        eventBasicInfo.type = type;
        eventBasicInfo.startDate = startDate;
        eventBasicInfo.endDate = endDate;
        return eventBasicInfo;
    }

    public EventExtendedInfo extendedInfo(UserBasicInfo userBasicInfo) {
        EventExtendedInfo eventExtendedInfo = new EventExtendedInfo();
        eventExtendedInfo.id = id;
        eventExtendedInfo.title = title;
        eventExtendedInfo.type = type;
        eventExtendedInfo.description = description;
        eventExtendedInfo.startDate = startDate;
        eventExtendedInfo.endDate = endDate;
        eventExtendedInfo.author = author;
        eventExtendedInfo.users = Collections.unmodifiableSet(users.stream()
                .map(EventUser::username)
                .collect(Collectors.toSet()));
        eventExtendedInfo.groups = Collections.unmodifiableSet(groups.stream()
                .map(EventGroup::groupName)
                .collect(Collectors.toSet()));
        eventExtendedInfo.deletionAllowed = isDeletionAllowed(userBasicInfo);
        return eventExtendedInfo;
    }

    public EventNotification eventNotification() {
        EventNotification eventNotification = new EventNotification();
        eventNotification.title = title;
        eventNotification.users = Collections.unmodifiableSet(users.stream()
                .map(EventUser::username)
                .collect(Collectors.toSet()));
        eventNotification.groups = Collections.unmodifiableSet(groups.stream()
                .map(EventGroup::groupName)
                .collect(Collectors.toSet()));
        return eventNotification;
    }

    public boolean isDeletionAllowed(UserBasicInfo userBasicInfo) {
        if (!isEventBasicType()) {
            return false;
        }
        if (author.equals(userBasicInfo.username())) {
            return true;
        }
        if (userBasicInfo.isManager()) {
            return groups.stream()
                    .map(EventGroup::groupName)
                    .anyMatch(groupName -> groupName.equals(userBasicInfo.group()))
                    || users.stream()
                        .map(EventUser::username)
                        .anyMatch(username -> username.equals(userBasicInfo.username()));
        }
        return false;
    }

    public boolean isUserInEvent(String username) {
        return users.stream()
                .map(EventUser::username)
                .anyMatch(eventUsername -> eventUsername.equals(username));
    }

    public boolean isGroupInEvent(String groupName) {
        return groups.stream()
                .map(EventGroup::groupName)
                .anyMatch(eventGroupName -> eventGroupName.equals(groupName));
    }
    
    public boolean isEventBasicType() {
        return type == TRAINING || type == CONFERENCE || type == MEETING || type == DELEGATION;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(id, event.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Event{" + "id=" + id + '}';
    }
}
