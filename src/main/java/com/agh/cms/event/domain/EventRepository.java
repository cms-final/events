package com.agh.cms.event.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Transactional
public interface EventRepository extends JpaRepository<Event, Integer> {

    //todo: still n+1 despite the fetch all properties...
    @Query("select e from Event e fetch all properties where e.type != 'WORKING_DAY' and " +
            "e.startDate between :startDate and :endDate or e.endDate between :startDate and :endDate")
    List<Event> findEventsDuringPeriodOfTime(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);

    @Query("select e from Event e inner join EventUser eu on e.id = eu.event.id where eu.username = ?1 ")
    List<Event> findByUsername(String username);

    @Query("select e from Event e inner join EventGroup eg on e.id = eg.event.id where eg.groupName = ?1")
    List<Event> findByGroupName(String groupName);
}
