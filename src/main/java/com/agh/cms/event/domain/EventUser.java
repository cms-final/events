package com.agh.cms.event.domain;

import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Access(AccessType.FIELD)
@Table(name = "event_user", schema = "event")
public class EventUser implements Serializable {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "event_id", referencedColumnName = "id")
    private Event event;

    @Id
    @Column(name = "username")
    private String username;

    private EventUser() {
    }

    EventUser(Event event, String username) {
        this.event = event;
        this.username = username;
    }

    String username() {
        return username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventUser eventUser = (EventUser) o;
        return Objects.equals(event, eventUser.event) &&
                Objects.equals(username, eventUser.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(event, username);
    }

    @Override
    public String toString() {
        return "EventUser{" +
                "event=" + event +
                ", username='" + username + '\'' +
                '}';
    }
}
