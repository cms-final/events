package com.agh.cms.event.domain;

import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Access(AccessType.FIELD)
@Table(name = "event_group", schema = "event")
public class EventGroup implements Serializable {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "event_id", referencedColumnName = "id")
    private Event event;

    @Id
    @Column(name = "group_name")
    private String groupName;

    private EventGroup() {
    }

    EventGroup(Event event, String groupName) {
        this.event = event;
        this.groupName = groupName;
    }

    String groupName() {
        return groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventGroup that = (EventGroup) o;
        return Objects.equals(event, that.event) &&
                Objects.equals(groupName, that.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(event, groupName);
    }

    @Override
    public String toString() {
        return "EventGroup{" +
                "event=" + event +
                ", groupName='" + groupName + '\'' +
                '}';
    }
}
