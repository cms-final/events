package com.agh.cms.event.domain;

import com.agh.cms.event.context.information.dto.EventNotification;

public interface NotificationsRepository {

    void publish(EventNotification eventNotification);
}
