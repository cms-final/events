package com.agh.cms.event.infrastructure.service;

import com.agh.cms.common.infrastructure.service.UserDeleteEventConsumerFactory;
import com.agh.cms.event.domain.Event;
import com.agh.cms.event.domain.EventRepository;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Configuration
class UsersDeleteConfiguration {

    UsersDeleteConfiguration(UserDeleteEventConsumerFactory consumerFactory, EventRepository eventRepository) throws IOException {
        UserDeleteEventConsumer consumer = new UserDeleteEventConsumer(consumerFactory.getChannel(), eventRepository);
        consumerFactory.createConsumer(consumer);
    }

    private static final class UserDeleteEventConsumer extends DefaultConsumer {

        private final EventRepository eventRepository;

        private UserDeleteEventConsumer(Channel channel, EventRepository eventRepository) {
            super(channel);
            this.eventRepository = eventRepository;
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
            String username = new String(body, StandardCharsets.UTF_8);
            List<Event> events = eventRepository.findByUsername(username);
            Set<Event> eventsToDelete = events.stream()
                    .filter(event -> !event.isEventBasicType())
                    .collect(Collectors.toSet());
            eventRepository.deleteAll(eventsToDelete);
        }
    }
}
