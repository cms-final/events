package com.agh.cms.event.infrastructure.service;

import com.agh.cms.common.domain.exception.RabbitException;
import com.agh.cms.event.context.information.dto.EventNotification;
import com.agh.cms.event.domain.NotificationsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
class NotificationsConfiguration {

    private static final String TOPIC = "events.add";

    private final Channel channel;

    NotificationsConfiguration(ConnectionFactory factory) throws Exception {
        Connection connection = factory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(TOPIC, false, false, false, null);
    }

    @Bean
    NotificationsRepository notificationsRepository() {
        return new RabbitPublisher(channel);
    }

    private static class RabbitPublisher implements NotificationsRepository {

        private static final Logger LOGGER = LoggerFactory.getLogger(RabbitPublisher.class);

        private final Channel channel;
        private final ObjectMapper mapper;

        private RabbitPublisher(Channel channel) {
            this.channel = channel;
            mapper = new ObjectMapper();
        }

        public void publish(EventNotification eventNotification) {
            try {
                byte[] message = mapper.writeValueAsString(eventNotification).getBytes();
                channel.basicPublish("actions_log", TOPIC, null, message);
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
                throw new RabbitException(e.getMessage());
            }
        }
    }
}
